import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesModule } from './articles/articles.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksModule } from './tasks/tasks.module';
import {environment} from "./environments/environment";

@Module({
  imports: [
    ArticlesModule,
    MongooseModule.forRoot(environment.DATABASE_CONNECTION),
    ScheduleModule.forRoot(),
    TasksModule,
  ],
})
export class AppModule {}
