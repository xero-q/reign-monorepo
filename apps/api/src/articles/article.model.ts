import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
  author: String,
  created_at: Date,
  story_title: String,
  title: String,
  objectID: String,
  story_url: String,
  url: String,
});

export interface Article extends Document {
  story_title: string;
  title: string;
  author: string;
  created_at: string;
  objectID: string;
  url: string;
  story_url: string;
}
