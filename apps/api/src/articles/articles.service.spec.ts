import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from './articles.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from './article.model';
import { HttpModule } from '@nestjs/common';
import { ArticlesController } from './articles.controller';
import {environment} from "../environments/environment";

describe('ArticlesService', () => {
  let service: ArticlesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule,
        MongooseModule.forRoot(environment.DATABASE_CONNECTION),
      ],
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getArticlesFromAPI', () => {
    it('should get at least 1 article from API', () => {
      const expectedLengthLimit = 1;
      return service.fetchFromAPI().then((result: any) => {
        expect(result.data.hits.length).toBeGreaterThanOrEqual(
          expectedLengthLimit,
        );
      });
    });
  });
});
