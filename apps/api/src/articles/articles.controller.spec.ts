import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from './article.model';
import { HttpModule } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import {environment} from "../environments/environment";

describe('ArticlesController', () => {
  let controller: ArticlesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule,
        MongooseModule.forRoot(environment.DATABASE_CONNECTION),
      ],
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).compile();

    controller = module.get<ArticlesController>(ArticlesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
