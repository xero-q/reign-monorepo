import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article } from './article.model';
import {environment} from "../environments/environment";

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('Article') private readonly articleModel: Model<Article>,
    private readonly http: HttpService,
  ) {}

  public findAllArticles(): Promise<Article[]> {
    return this.articleModel.find().exec();
  }

  public addAllArticles(): Promise<void> {
    return this.fetchFromAPI().then(async (result: any) => {
      for (const article of result.data.hits) {
        const foundArticle = await this.findArticle(article.objectID);
        if (!foundArticle) {
          await this.addArticle(article);
        }
      }
    });
  }

  public fetchFromAPI(): Promise<any> {
    return this.http.get(environment.API_URL).toPromise();
  }

  private async addArticle(article: Article): Promise<Article> {
    const newArticle = new this.articleModel(article);
    return await newArticle.save();
  }

  private async findArticle(objectID: string): Promise<Article> {
    return await this.articleModel.findOne({ objectID: objectID }).exec();
  }
}
