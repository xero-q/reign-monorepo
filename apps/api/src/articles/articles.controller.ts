import { Controller, Get } from '@nestjs/common';
import { ArticlesService } from './articles.service';

@Controller('/api/v1/articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  async findAll() {
    return await this.articlesService.findAllArticles();
  }
}
