import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { ArticlesModule } from '../articles/articles.module';

@Module({
  imports: [ArticlesModule],
  providers: [TasksService],
})
export class TasksModule {}
