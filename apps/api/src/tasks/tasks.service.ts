import { Injectable } from '@nestjs/common';
import { Interval, Timeout } from '@nestjs/schedule';
import { ArticlesService } from '../articles/articles.service';

@Injectable()
export class TasksService {
  constructor(private articlesService: ArticlesService) {}

  @Interval(3600 * 1000) //It runs every 3600 seconds = 1 hour
  async fetchArticles() {
    await this.articlesService.addAllArticles();
  }

  @Timeout(0)
  async startUp() {
    await this.fetchArticles();
  }
}
