import { Test, TestingModule } from '@nestjs/testing';
import { TasksService } from './tasks.service';
import { ArticlesModule } from '../articles/articles.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from '../articles/article.model';
import { HttpModule } from '@nestjs/common';
import {environment} from "../environments/environment";

describe('TasksService', () => {
  let service: TasksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule,
        MongooseModule.forRoot(environment.DATABASE_CONNECTION),
        ArticlesModule,
        MongooseModule.forRoot(environment.DATABASE_CONNECTION),
      ],
      providers: [TasksService],
    }).compile();

    service = module.get<TasksService>(TasksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
