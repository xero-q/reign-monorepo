export interface Article {
  _id: string;
  story_title: string;
  title: string;
  author: string;
  created_at: string;
  objectID: string;
  url: string;
  story_url: string;
}

