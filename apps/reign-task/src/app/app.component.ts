import { Component } from '@angular/core';

@Component({
  selector: 'reign-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
}
