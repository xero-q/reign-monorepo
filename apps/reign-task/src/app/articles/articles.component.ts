import {Component, OnInit} from '@angular/core';
import {Article} from '../interfaces/article.interface';
import {ApiService} from '../services/api.service';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'reign-app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  articles: Article[] = [];
  deleted: string[] = [];

  constructor(
    private apiService: ApiService,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.deleted = this.getDeletedArticles();
    this.apiService.getArticles().toPromise()
      .then((result: Article[]) => {
        this.articles = result.filter((i: Article) => i.story_title || i.title )
          .filter((i: Article) => {
            return !this.deleted.includes(i.objectID);
          })
          .sort((a: Article, b: Article) => -a.created_at.localeCompare(b.created_at));
      }).catch((error) => {
      console.error(error);
    });
  }

  doOpen(id: string): void {
    const article: Article | undefined = this.articles.find(i => i._id === id);

    if (article !== null) {
      const url = article?.story_url ? article?.story_url : article?.url;

      if (url) {
        window.open(url, '_blank');
      } else {
        alert(`This article doesn't have a URL`);
      }
    }
  }

  doDelete(id: string): void {
    if (confirm('Are you sure you want to delete this article?')) {
      const article: Article | undefined = this.articles.find(i => i._id === id);

      if (article) {
        this.deleted.push(article.objectID);
        localStorage.setItem('deleted', JSON.stringify(this.deleted));
        this.articles = this.articles.filter(i => i._id !== id);
      }
    }
  }

  getDeletedArticles(): string[] {
    const deleted = localStorage.getItem('deleted');

    if (deleted !== null) {
      return JSON.parse(deleted);
    } else {
      return [];
    }
  }

  getFormattedDate(date: string): string | null {
    const today = new Date();
    const passedDate = new Date(date);

    if (today.getFullYear() === passedDate.getFullYear()
      && today.getMonth() === passedDate.getMonth()
      && today.getDate() === passedDate.getDate()) { // It's current day date
      return this.datePipe.transform(date, 'hh:mm a');
    } else {
      return moment(date).fromNow();
    }
  }
}
