import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesComponent } from './articles.component';


@NgModule({
  declarations: [ArticlesComponent],
  imports: [
    CommonModule,
    ArticlesRoutingModule
  ],
  providers: [DatePipe]
})
export class ArticlesModule { }
